all:
	cd my/; make
	cd src/; make

clean:
	cd my/; make clean
	cd src/; make clean

fclean:
	cd my/; make fclean
	cd src/; make fclean

re:
	cd my/; make re
	cd src/; make re
