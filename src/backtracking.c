/*
** backtracking.c for backtracking in /home/delafo_c/rendu/sudoki-bi/src
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Sat Mar  1 05:23:22 2014 delafo_c
** Last update Sun Mar  2 16:33:19 2014 delafo_c
*/

#include <stdlib.h>
#include "../include/my.h"
#include "sudoku.h"

int		is_valid_number(char **grids, char c, int i, int j)
{
  int		ret;

  ret = 0;
  ret = ((is_nb_in_line(grids, c, i)) + (is_nb_in_column(grids, c, j))
	 + (is_nb_in_square(grids, c, i, j)));
  if (ret == TRUE)
    return (TRUE);
  return (FALSE);
}

int            is_nb_in_line(char **grids, char c, int i)
{
  int           j;

  j = 0;
  while (j < 9)
    {
      if (grids[i][j] == c)
        return (FALSE);
      j++;
    }
  return (TRUE);
}

int             is_nb_in_column(char **grids, char c, int j)
{
  int           i;

  i = 0;
  while (i < 9)
    {
      if (grids[i][j] == c)
        return (FALSE);
    }
  return (TRUE);
}

int             is_nb_in_square(char **grids, char c, int i, int j)
{
  int           line;
  int           column;

  line = i - (i%3);
  column = j - (j%3);
  while (line < (line + 3))
    {
      while (column < (column + 3))
        {
          if (grids[line][column] == c)
            return (FALSE);
          column++;
        }
      line++;
    }
  return (TRUE);
}

int		bt_recurs(char **grids, int i, int j)
{
  int		ret;

  if (i == 9 && j == 9)
    ret = bt_check_grids(grids, 9, 9);
  if (j == 9)
    ret = bt_check_grids(grids, i++, 0);
  else  // ELSE ??
    ret = bt_check_grids(grids, i, j++);
  return (ret);
}

int		bt_check_grids(char **grids, int i, int j)
{
  char		c;

  printf("tarace");

  if (i == 9 && j == 9)
    return (TRUE);
  if (grids[i][j] != '0')
    {
      bt_check_grids(grids, i, j++);
    }
    //    return (bt_recurs(grids, i, j));
  else
    {
      c = '1';
      while ((is_valid_number(grids, c, i, j)) && c <= '9' && c >= '1')
	c++;
      printf("%c ", c);
      grids[i][j] = c;
      if (bt_recurs(grids, i, j) == TRUE)
	return (TRUE);
    }
  grids[i][j] = '0';
  return (FALSE);
}
