/*
** sudoku.h for sudoku header file in /home/loisel_k/Taff/C/C-Prog-Elem/B2/Rush2/src
** 
** Made by Kevin LOISELEUR
** Login   <loisel_k@epitech.net>
** 
** Started on  Sat Mar  1 20:07:42 2014 Kevin LOISELEUR
** Last update Sun Mar  2 16:22:16 2014 Kevin LOISELEUR
*/

#ifndef _SUDOKU_H_
# define _SUDOKU_H_

# define NB_LINE	10
# define NB_GRID	10
# define TRUE		0
# define FALSE		1


/*
** PARS
*/
char			**recup_maps(void);
int			recup_read(char **tab_map);
char			*analyse_line(char *s);
void			tab_copy(char *tmp, char **tab_map, int i);

/*
** DISPLAY
*/
void			aff_grids(char **grids);
void			aff_tab(void);
void			aff_delim(void);

/*
** CALC
*/
int			is_nb_in_line(char **grids, char c, int i);
int			is_nb_in_column(char **grids, char c, int j);
int			is_nb_in_square(char **grids, char c, int i, int j);
int			bt_check_grids(char **grids, int i, int j);
int			bt_recurs(char **grids, int i, int j);
#endif /* _SUDOKU_H_ */
