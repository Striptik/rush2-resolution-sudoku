/*
** aff.c for aff in /home/delafo_c/rendu/sudoki-bi/src
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Fri Feb 28 23:43:47 2014 delafo_c
** Last update Sat Mar  1 23:40:49 2014 Kevin LOISELEUR
*/

#include "../include/my.h"
#include "../include/list.h"
#include "sudoku.h"

void            aff_grids(char **grids)
{
  int           i;
  int           j;

  i = 0;
  aff_tab();
  while (i < 9)
    {
      my_printf("|");
      j = 0;
      while (j < 9)
	{
	  my_printf(" %c", grids[i][j]);
          j++;
	}
      my_printf("|\n");
      i++;
    }
  aff_tab();
}

void		aff_tab(void)
{
  my_printf("|------------------|\n");
}

void		aff_delim(void)
{
  my_printf("####################\n");
}
